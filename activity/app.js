let port = 3000;

const express = require('express');
const application = express();


let coursesCollection = [];


application.listen(port, () => {
	console.log(`app is running on port ${port}`);
});

application.get('/', (req, res) => {
	res.write("Welcome to Miguel's Website");
	res.end();
});

application.get('/courses', (req, res) => {
	res.write(JSON.stringify(coursesCollection));
	res.end();
});

application.post('/course', (req, res) => {
	let reqBody = '';
	let objCreated = [];

	req.on('data', (dataFromPostman) => {
			reqBody += dataFromPostman;
			objCreated = JSON.parse(reqBody);
			coursesCollection.push(objCreated);
		});

		req.on('end', function() {
			res.writeHead(201, {'Content-Type': 'application/JSON'});
			res.write("The object created is: \n" + JSON.stringify(objCreated));
			res.end();
		});
});

application.put('/course', (req, res) => {
	let reqBody = '';
	let objCreated = [];

	req.on('data', (dataFromPostman) => {
			reqBody += dataFromPostman;
			objCreated = JSON.parse(reqBody);
			for (let i = 0; i < coursesCollection.length ; i++) {
				if (coursesCollection[i].name === objCreated.name) {
					coursesCollection[i].price = objCreated.price;
				}
			}
			
		});

		req.on('end', function() {
			res.writeHead(201, {'Content-Type': 'application/JSON'});
			res.write("The object updated is: \n" + JSON.stringify(objCreated));
			res.end();
		});
});